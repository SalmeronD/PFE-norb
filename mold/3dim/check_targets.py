import lmdb
import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Make sure that caffe is on the python path:
caffe_root = '/home/diego/.caffe/caffe/'
import sys
sys.path.insert(0, caffe_root + 'python')
import caffe

# Checking an image together with its label in the normal
# training monocular database.

item_id = 2

print('Opening lmdb')
dp_path = caffe_root + 'pfe/norb/mono-uni/norb_train_lmdb'
if not os.path.exists(dp_path):
    raise Exception(dp_path + ' does not exist.')
mdb_env = lmdb.open(dp_path, map_size=int(1e12))
with mdb_env.begin() as mdb_txn:
    cursor = mdb_txn.cursor()
    cursor.get('{:0>8d}'.format(item_id))
    value = cursor.value()
    key = cursor.key()
    print(key)
    datum = caffe.proto.caffe_pb2.Datum()
    datum.ParseFromString(value)
    print(datum.channels, datum.height, datum.width)

    image = caffe.io.datum_to_array(datum)
    # print(image)
    image = image[0]
    # print(image)
    # print(np.shape(image))
    # plt.imshow(image, cmap=cm.gray)
    # plt.show()
    print('The label for this image:')
    print(datum.label)

# Checking the value corresponding to the previous image in the
# target database.

print('Opening lmdb')
tar_path = 'norb_train_target_lmdb'
if not os.path.exists(tar_path):
    raise Exception(tar_path + ' does not exist.')
mdb_env = lmdb.open(tar_path, map_size=int(1e12))
with mdb_env.begin() as mdb_txn:
    cursor = mdb_txn.cursor()
    keys = []
    datums = []
    targets = np.zeros((5, 3))
    labels = np.zeros(5, dtype=int)
    for i in range(5):
        cursor.get('{:0>8d}'.format(i))
        value = cursor.value()
        keys.append(cursor.key())
        datums.append(caffe.proto.caffe_pb2.Datum())
        datums[i].ParseFromString(value)
        targets[i] = \
                caffe.io.datum_to_array(datums[i]).reshape(3)
        labels[i] = datums[i].label


colors = ['#ff0000', '#ffff00', '#00ff00', '#00ffff', '#0000ff']
categories = ('Animal', 'Human', 'Airplane', 'Truck', 'Car')
f = plt.figure()
pic = f.add_subplot(111, projection='3d')
for i in range(5):
    pic.scatter(targets[i, 0], targets[i, 1], targets[i, 2], 'o',
                c=colors[i], label=categories[i])
plt.grid()
plt.legend(numpoints=1)
plt.savefig('targets.png')
np.save('targets', targets)
