import numpy as np
import lmdb
import os

# Make sure that caffe is on the python path:
caffe_root = '/home/diego/.caffe/caffe/'
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe


def filterData(data, labels, k):
    '''Keep k samples in each class
    '''
    allIndex = []
    allClass = np.unique(labels)
    for l in allClass:
        theIndex = list(np.where(labels == l))[0]
        print (len(theIndex))
        allIndex.extend(theIndex[:k])
    labels = labels[allIndex]
    data = data[:, allIndex]
    return data, labels


iters = 5000
nS = 2
PRETRAINED_FILE = 'tsmlCNNmodel_iter_' + str(iters) + \
                  '.caffemodel'
MODEL_FILE = 'tsmlCNN.prototxt'
caffe.set_mode_cpu()
net = caffe.Net(MODEL_FILE, PRETRAINED_FILE, caffe.TEST)

# Read training data

TRAIN_DATA_FILE = caffe_root + \
                  'data/norb/smallnorb-5x46789x9x18x6x2x96x96-training-dat.mat'
TRAIN_LABEL_FILE = caffe_root + \
                   'data/norb/smallnorb-5x46789x9x18x6x2x96x96-training-cat.mat'
n = 24300

with open(TRAIN_DATA_FILE, 'rb') as f:
    f.seek(24, 0)  # Skip the header
    data = np.fromstring(f.read(n*2*96*96), dtype=np.uint8)

with open(TRAIN_LABEL_FILE, 'rb') as f:
    f.seek(20, 0)  # Skip the header
    labels = np.fromstring(f.read(n*4), dtype=np.int32)

# Keeping only one image from every pair, in order to carry out a
# monocular classification.

mono_images = np.take(data.reshape(n*2, 96, 96), np.arange(1,
                                                           n*2,
                                                           2), 0)
data = mono_images.reshape(n, 96*96).T
raw_data, train_labels = filterData(data, labels, nS)

# Reshape and preprocess

categories = 5
caffe_in = raw_data.T.reshape(nS*categories, 1, 96, 96) * 0.00390625
out = net.forward_all(data=caffe_in)
train_feat = out['feat']
train_centers = np.zeros((categories, 3))
for i in range(categories):
    train_centers[i, :] = np.mean(train_feat[train_labels == i,
                                             :], 0)
train_centers /= np.sqrt(np.sum(train_centers**2, axis=1))[:,
                                                           np.newaxis]

# Writing targets in database
# For every image in the database, its corresponding target is
# written to the database.

print('----------')
print('Opening LMDB')
dp_path = 'norb_train_target_lmdb'
if (not os.path.exists(dp_path)):
    os.mkdir(dp_path)
mdb_env = lmdb.open(dp_path, map_size=int(1e12))
datum = caffe.proto.caffe_pb2.Datum()
datum.channels = 3
datum.height = 1
datum.width = 1

mdb_txn = mdb_env.begin(write=True)

for i in range(n):
    label = labels[i]
    target = np.zeros((datum.channels, datum.height,
                       datum.width))
    target[:, 0, 0] = train_centers[label]
    datum = caffe.io.array_to_datum(target, int(label))
    keystr = '{:0>8d}'.format(i)
    mdb_txn.put(keystr, datum.SerializeToString())
    if (i+1) % 1000 == 0:
        print(str(i+1) + ' data passed')
        mdb_txn.commit()
        mdb_txn = mdb_env.begin(write=True)

if n % 1000 != 0:
    mdb_txn.commit()

mdb_env.close()
