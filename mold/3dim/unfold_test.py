import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from sklearn import neighbors
knn = neighbors.KNeighborsClassifier()

# Make sure that caffe is on the python path:
caffe_root = '/home/diego/.caffe/caffe/'
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe
    
# Decrease if you want to preview during training
iters = 10000
PRETRAINED_FILE = 'CNNmodel_iter_' + str(iters) + '.caffemodel'
MODEL_FILE = 'CNN.prototxt'

caffe.set_mode_cpu()
net = caffe.Net(MODEL_FILE, PRETRAINED_FILE, caffe.TEST)

# ---------Read in test data --------#
TEST_DATA_FILE = caffe_root + \
'data/norb/smallnorb-5x01235x9x18x6x2x96x96-testing-dat.mat'
TEST_LABEL_FILE = caffe_root + \
'data/norb/smallnorb-5x01235x9x18x6x2x96x96-testing-cat.mat'
n = 24300

with open(TEST_DATA_FILE, 'rb') as f:
    f.seek(24, 0)  # Skip the header
    raw_data = np.fromstring(f.read(n*2*96*96), dtype=np.uint8)

with open(TEST_LABEL_FILE, 'rb') as f:
    f.seek(20, 0)  # Skip the header
    test_labels = np.fromstring(f.read(n*4), dtype=np.int32)

# Keeping only one image from every pair, in order to carry out a
# monocular classification.

mono_images = np.take(raw_data.reshape(n*2, 96, 96), np.arange(1,
                                                               n*2,
                                                               2),
                                                               0)

# Reshape and preprocess
caffe_in = mono_images.reshape(n, 1, 96, 96) * 0.00390625
out = net.forward_all(data=caffe_in)

test_feat = out['ip2']

colors = ['#ff0000', '#ffff00', '#00ff00', '#00ffff', '#0000ff']
categories = ('Animal', 'Human', 'Airplane', 'Truck', 'Car')

f = plt.figure(figsize=(16, 9))
pic = f.add_subplot(111, projection='3d')
for i in range(5):
    pic.scatter(test_feat[test_labels == i, 0].flatten(),
                test_feat[test_labels == i, 1].flatten(),
                test_feat[test_labels == i, 2].flatten(),
                '.', c=colors[i], label=categories[i])
plt.legend(numpoints=1)
plt.grid()
plt.savefig('test_map_before_norm.png')
# plt.show()

# After unit normalization
test_feat /= np.sqrt(np.sum(test_feat**2, axis=1))[:, np.newaxis]
f = plt.figure(figsize=(9, 9))
pic = f.add_subplot(111, projection='3d')
for i in range(5):
    pic.scatter(test_feat[test_labels == i, 0].flatten(),
                test_feat[test_labels == i, 1].flatten(),
                test_feat[test_labels == i, 2].flatten(),
                '.', c=colors[i], label=categories[i])
plt.grid()
plt.savefig('test_map_after_norm.png')
# plt.show()

# cut the line
theta1 = np.arctan2(test_feat[:, 0], test_feat[:, 1]) % (2*np.pi)
cost1 = np.cos(theta1)
theta2 = np.pi - np.arctan2(test_feat[:, 1] / cost1, test_feat[:,
                                                               2])
f = plt.figure(figsize=(16, 5))
for i in range(5):
    the_samples1 = theta1[test_labels == i]
    the_samples2 = theta2[test_labels == i]
    plt.plot(the_samples1.flatten(), the_samples2.flatten(), '.',
             c=colors[i])
    plt.ylim([0, np.pi])
    plt.xlim([0, 2*np.pi])
    plt.grid()
    plt.savefig('test_map_after_reduction.png')
