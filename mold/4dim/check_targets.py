import lmdb
import os
import numpy as np

# Make sure that caffe is on the python path:
caffe_root = '/home/diego/.caffe/caffe/'
import sys
sys.path.insert(0, caffe_root + 'python')
import caffe

print('Opening lmdb')
tar_path = 'norb_train_target_lmdb'
if not os.path.exists(tar_path):
    raise Exception(tar_path + ' does not exist.')
mdb_env = lmdb.open(tar_path, map_size=int(1e12))
with mdb_env.begin() as mdb_txn:
    cursor = mdb_txn.cursor()
    keys = []
    datums = []
    targets = np.zeros((5, 4))
    labels = np.zeros(5, dtype=int)
    for i in range(5):
        cursor.get('{:0>8d}'.format(i))
        value = cursor.value()
        keys.append(cursor.key())
        datums.append(caffe.proto.caffe_pb2.Datum())
        datums[i].ParseFromString(value)
        targets[i] = \
                caffe.io.datum_to_array(datums[i]).reshape(4)
        labels[i] = datums[i].label

np.save('targets', targets)
