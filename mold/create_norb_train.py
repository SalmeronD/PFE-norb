import numpy as np
import lmdb
import os

# Make sure that caffe is on the python path:
caffe_root = '/home/diego/.caffe/caffe/'
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe

# Obtaining the data

TRAIN_DATA_FILE = caffe_root + \
                  'data/norb/smallnorb-5x46789x9x18x6x2x96x96-training-dat.mat'
TRAIN_LABEL_FILE = caffe_root + \
                   'data/norb/smallnorb-5x46789x9x18x6x2x96x96-training-cat.mat'
n = 24300

with open(TRAIN_DATA_FILE, 'rb') as f:
    f.seek(24, 0)  # Skip the header
    data = np.fromstring(f.read(n*2*96*96), dtype=np.uint8)

with open(TRAIN_LABEL_FILE, 'rb') as f:
    f.seek(20, 0)  # Skip the header
    labels = np.fromstring(f.read(n*4), dtype=np.int32)

# Keeping only one image from every pair, in order to carry out a
# monocular classification.

mono_images = np.take(data.reshape(n*2, 96, 96), np.arange(1,
                                                           n*2,
                                                           2), 0)

# Database writing

dp_path = 'norb_train_lmdb'
if (not os.path.exists(dp_path)):
    os.mkdir(dp_path)
mdb_env = lmdb.open(dp_path, map_size=int(1e12))
datum = caffe.proto.caffe_pb2.Datum()
datum.channels = 1
datum.height = 96
datum.width = 96

mdb_txn = mdb_env.begin(write=True)

for i in range(n):
    label = labels[i]
    image = np.zeros((datum.channels, datum.height, datum.width))
    image[0] = mono_images[i]
    datum = caffe.io.array_to_datum(image, int(label))
    keystr = '{:0>8d}'.format(i)
    mdb_txn.put(keystr, datum.SerializeToString())
    if (i+1) % 1000 == 0:
        print(str(i+1) + ' data passed')
        mdb_txn.commit()
        mdb_txn = mdb_env.begin(write=True)

if n % 1000 != 0:
    mdb_txn.commit()

mdb_env.close()
