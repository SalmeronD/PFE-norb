* Papers using NORB
- Learning Methods for Generic Object Recognition with Invariance to
  Pose and Lightning. /cvpr-2004
- Dimensionality Reduction by Learning an Invariant
  Mapping. /siamese-review/2006_siameseCNN_dimensionality_reduction
* About the database 
Images of toys. 10 instances of 5 generic categories: four-legged
animals, human figures, airplanes, trucks and cars.
Each object instance has 9 azimuths, 36 angles and 6 lightnings
conditions.
Images are paired, as two cameras took the photos under the same
conditions.
179 GB of raw data -> If we want to train and test over the whole
dataset, we should think of an online solution. There is not enough
space.
There are four datasets:
- Normalized-uniform dataset: Normal images, no
  additions. Called the "small NORB database".
- Jittered-uniform dataset: Images with random perturbations.
- Jittered-textured dataset: Random perturbations and natural
  backgrounds.
- Jittered-cluttered dataset: Random perturbations, natural
  backgrounds and distracting objects.
There are some experiments reported on [cvpr-2004] over the NORB
database using Convolutional Nets. Maybe make the same experiments
with the TSML-Hybrid approach.
* Observations or unanswered questions
Caffe uses two kinds of databases in which data is stored: LevelDB
and LMDB. Both have Python bindings. When to use each? Or are they
interchangeable?

convert_mnist_tiny seems to just create the database with following
the index matrix for mini-batch gradient.

np.arange is much faster tan built-in range.

Batch sizes matter? Should I change them?
Why ratio in convert_norb_tiny is set to 1?
* MNIST code analysis
** convert_mnist_tiny.py
LevelDB is used.
Why n_pairs in convert_mnist_tiny is 10000? Why ratio is 1? Is it
because we will be taking just two samples from each class? What is
niters?
* Implementation
** convert_norb_tiny.py
First do the experiments on the binocular, normalized-uniform
dataset.
Follow the network structure recommended in the paper, like we
followed MNIST's in its experiments.
Compare with results, but taking into account that those are not
visualizable spaces.
- Created the folder norb in caffe_root/data
- Created the script get_norb.sh for downloading and unzipping the
  data.

When reading the files, we have to take into account the header,
which is, in a C-structure definition:

struct header {
int magic; // 4 bytes
int ndim; // 4 bytes, little endian
int dim[3];
};

When dimensions are more than 3, further information about
dimensions follows the header. Otherwise, directly the data
matrix.

Small NORB consists of 24300 image pairs. I am going to try to do it
in the binocular way. Getting the pairs of images and concatenating
with np.concatenate does the job. I supposed that convolutional nets
must have a joined image with the two perspectives as input.

Categories are coded as 4-byte integers. So, to correctly read it, we
will use dtype=int32.

How many number of pairs should we use?

I am going to code in a "Matlab" way, by writing scripts to be
executed in IPython, that can, eventually, call some other
functions. Maybe later I will have to "translate" from script to
function to make an easier execution process.

In MNIST, images are concatenated, with a similar function named
hstack. In here, we would have to concatenate the already
concatenated images:
MNIST -> An image of a 2 and that of a 6 become a unique image
having a 2 and a 6.
NORB -> An image of a human is paired with its binocular to make
a unique image with two humans. Then this is paired again with
another binocular image, thus forming a sequence of, for
example, human-human-truck-truck

Passed 10000 data to the norb database.

In the paper it is said that, from the 8 feature extractors that
the convolutional layer has, two of them act on the left image,
two on the right, and the rest in both. Maybe each image should
arrive independently to the net, and not concatenated?

I am changing the approach: First I will make the monocular
classification and later the binocular approach.

When reading the NORB labels, it should be fread(n*4) instead of
f.read(n), because they are int32.

In the monocular case: Use every image or just take one in every
pair? LeCun uses the first approach.

Changed it to manipulate data exactly as Lilei (Just to make the
training work)
** train_tsmlCNN2.sh
As stated before, we are going to use a siamese architecture
similar to that used in the paper "cvpr"

tsmlCNN2_train definition: 
- Do we have to add the transform_param or that was ad-hoc for
  MNIST?
  - It is the normalization of the intensity values, which go
    from 0 to 255. The value there is just 1/256, so we must
    keep it.
- Batch_size must be 10, as in MNIST?
- Is there need for an Inner Product layer? The third
  convolution seems to be completely connected to the previous layer.
  - Following LeNet5 example (depicted in the thesis) I add a
    fully-connected (Inner product) layer.
  - In the paper there is only one mention to an "output layer",
    but no information on its dimensions.
See the implementation of LeNet-5. There each feature extractor
of each convolutional layer takes input from a subset of the
input. That is what we want for the binocular case.

It will be simpler to use the same network as in MNIST. The
parameters of each layer must be adequately changed in order to
adapt to the input size.

tsmlCNN2_train took 2:13 to make 3000 iterations. We must reduce
the number of iterations to make this more quickly.

Possible line of work: What requires more computing in a NN, the
number of parameters or the number of connections? 

For each particular case, the choice of a suitable net is very
important. Using a net created for the MNIST dataset could work
badly on NORB, so it's better to use the recommended
architecture for the database, which in the case of NORB is
LeNet7 (http://www.cs.nyu.edu/~yann/research/norb/)

Use the same number of feature maps for both the monocular and
the binocular case.

I removed the inner product from MNIST and replaced it by a
third convolutional layer, which seemed the right implementation
of LeNet7. After it, I place the feat and MTriangularLoss
layers, in order to make the architecture suitable for the
Triangular Similarity problem.

The convoutional layer gave some problems with the dimensions
when executing the net with Caffe. Replacing it with an Inner
Product, but not adding (at least yet) the ReLU.

tsmlCNN2 took 13:10 min to make 4000 iterations.
tsmlCNN2 took 15:42 min to make 4000 iterations.

Changed the source! It was taking data from the binocular set,
causing every error!

tsmlCNN2 took 7:19 min to make 4000 iterations.
** form_train_target2.py
There is an error concerning dimensions when training the
model. Souce param, which is the norb_train_pairs_leveldb, has a
shape of 100 360, and target param shape is 100 216. Error
arises at layer ip1, but the 216 is from the blob pool2.
Maybe changing the ip1 output size to 80?

Adding a ReLU activation layer, to see if that solves the
problem. It didn't

The strides were causing the bad behaviour. Strides in
convolutional layers must be 1. Strides in pooling layers must
be equal to the kernel size, in order to make them not
overlapping. 

Once the changes are done, the net must be trained again, as
form_train_target2.py uses the model previously trained, not the
file.

I changed the slice_dim parameter in the Slice layer from 1 to
0, in case the problems comes from thinking that data is
displayed the other way around.

Output from leveldb has shape 10, 2, 96, 192. It should be 10,
2, 96, 96, as it then slices in axis 1, leaving two blobs (data
and data_p) of dimensions 10, 1, 96, 192, where we should have
10, 1, 96, 96.

Database is not correctly written!
** check_targets.py
It relies at some point in a database given by Caffe, in the
MNIST example. How to make this database for NORB? I think this
is the normal training MNIST database, without pairs.

There are only five unique target points in all the target
database, so that seems to be correct.

Targets for 'Human' and 'Animal' are very close together, which
may result in a bad classificatio
** train_CNN2.sh
Two databases are used: That of the whole dataset, for training,
and that of targets, which is used as label.

The net is equal to tsmlCNN2_train (it takes its weights), but
substituting the "feat" layer of this one by a second Inner
Product layer. A "Flatten" layer is added for the targets.

Parameters in layers have no longer names, as these are not
siamese architectures.

Check what the "Flatten" layer does exactly.

Training of CNN2_train took 58:17
** draw_final_shape2.py
It seems that both train and test sets are composed of 24300
binocular images.

Accuracy of the train set: 0.99646090535
Accuracy of the test set: 0.810617283951

There is a possible overfitting in the train test. Categories
are very well separated, but when new images arrive, they spread
over all the spectrum
** 3 dimension output
tsmlCNN3_train took 7:28 min to train.
CNN3_train took 55:54 min to train. Every train net output #0
has value -nan...
tsmlCNN3 training gives normal values as
outputs. form_train_target3, which relies in tsmlCNN3, seems to
be correctly working.
* Improving classification
I will create a folder called historical, in which I will store
each important version of the code in the corresponding
subfolder. Each of these subfolders will contain a description
of the most important parameters of this version, the code files
and the result files (mostly images)
** Version 2
Training of tsmlCNN2_train took 1:51
Points of truck and car are very close together. I am continuing
with the whole classification and later make another version
with 3000 iterations in tiny scale.
Training of the CNN2 also gives -nan values
Training of tsmlCNN2_train with 3000 iterations took 2:54
Training of tsmlCNN2_train with 4000 iterations took 3:54
Training of tsmlCNN2_train with 5000 iterations took 4:39
4000 iterations seems to give the best result. We could check
the 4500 value or move onto new values to change.
When done with 4000 iterations, and after doing the experiments
on the targets for several values of the iterations, the
training of CNN began to work.
Training of CNN2_train with 10000 iterations took 29:53.
Training accuracy as calculated in draw_final_shape:
0.992139917695
Testing accuracy as calculated in draw_final_shape: 0.739259259259
* Automating the classification
** Variable values
- One of the NORB variations
- Monocular or binocular images
- Number of training images (mono case: 24300 or 48600)
- Number of similar pairs
- Number of feature extractors in the net
- Number of samples from each class in the tiny training
- Number of iterations in tiny phase
** Values changing process
- create_norb_train_mono.py:
  - Creates training database
  - Changing n and the mono_images increases the training size
  - Name of the database must be taken into account!
- convert_norb_tiny-mono.py:
  - Creates the database of pairs
  - Changing n according to the training size
  - Changing n_pairs changes the number of similar pairs
    calculated.
  - Changing nS changes the number of samples taken from each
    class.
  - Name of the database must be taken into account!
- tsmlCNN2_train.prototxt:
  - The tiny phase net to be trained.
  - data_param's source must reflect the adequate database
  - Convolutional layers with their feature
    extractors. Attention to the paired layers!
  - Feature layer's output number. Says the output dimension in
    which we will be working.
- tsmlCNN2.prototxt:
  - The model net used in form_train_target2.py
  - Convolutional layers with their adequate number of feature
    extractors.
  - Feature layer output number set to the dimension output.
- tsmlCNN2_solver.prototxt:
  - The solver for the training tiny net
  - Number of iterations in the max_iter and snapshot fields.
- train_tsmlCNN2.sh:
  - The training script
  - Nothing to be changed if net names are kept
- form_train_target.py:
  - Creates the database of the targets.
  - iters must reflect the correct value
  - nS mst reflect the correct value
  - n must reflect the correct value
  - mono_images only in mono mode
  - datum.channels must adequate to the feature number of
    dimensions.
- check_targets.py:
  - Check the validity of the database and graphs the objective
    targets.
  - As long as the database name is not changed, everythong OK.
- CNN2_train.prototxt:
  - Large scale training net
  - Good source for data. Use relative path instead of absolute.
  - Correct number of feature extractors in conv1 and conv2
  - Correct number of output dimensions in ip2.
- CNN2.prototxt
  - Large scale training model net
  - Correct number of feature extractors in conv1 and conv2
  - Correct number of output dimensions in ip2.
- CNN2_solver.prototxt:
  - Solver for the large scale training
  - Nothing to change, d'abord.
- train_CNN2.sh:
  - Number of iters in the weights parameter
- draw_final_shape.py:
  - mono_images only in mono mode.
  - Save the kNN classification accuracy somewhere!
- unfold_test.py:
  - mono_images only in mono mode.
** Steps in the training
Here I am going to clearly separate the steps of the process and
numerate them in order to have an easy way of referring to them:
1. Creation of training database
2. Creation of tiny phase database
3. Training of tiny phase
4. Target database creation
5. Targets checking
6. Final shape drawing
7. Test unfolding
** Folder hierarchy
In the NORB folder, for each change in the database, we will
have a different folder (i.e.: mono-uni is storing the
monocular, uniform dataset).
Inside this folder the script to create the database containing
the training data, the database itself, and a folder for every
experiment with changing values.
Inside the experiment folder, the script for creating the tiny
phase database along with the database itself, and a folder for
every output dimension.
Graphically is something like:

- NORB
  - mono-uni
    - create_norb_train.py
    - norb_train_lmdb
    - 1000-4-4-12
      - convert_norb_tiny.py
      - norb_train_pairs_lmdb
      - 2dim

In CNN2_train.prototxt, source is set to
$NORB/mono-uni/norb_train_lmdb. Adapting this code to other
databases will need to change this path.
** Improvements
- Use the stdout argument in call to write outputs to files.
- Make it more robust to calls from wrong directories.
- In run_net, not running if norb_train_pairs_leveldb is already
  defined, as that directory has very possibly already been run.
- Create only one database for each case of samples. When the
  number of samples is equal, the database is also equal.
- Files like check_targets.py don't need to be repeated for
  every experiment.
- Title of each subplot only containing the variable values
- Improve the figure quality
- Automatically get the best result from the values stored for
  targets by computing the distance.
- Create options to overrun or not the current files.
- Make an updating script that obtains the date of the files and
  checks if the mold has been changed after that, Then update.
- When defining a net for which files already exist, not
  eliminating the whole diretory, as this erases the previous
  results. These should be erased only when carrying out a new
  classification.
- Take care when directory given by argument does not exist in
  run_net()
- In run_net(), there is a bug when calling with two dimensions
  set to True. In the second dimension the command fails because
  it is not one of those in the list. In a previous run, it was
  command 'p'.
- Remove the comment blocks in 4dim/knn_on_output and
  4dim/unfold_view.
- When calling get_target_results as a script indicating
  conv1='4', it takes value iters='conv1='4'', so no directories
  are found with that description.
- In get_target_results, sometimes more than one figure is plotted.
** Notes
There is no chek_targets.py in 4dim
Instead of draw_final_shape we have knn_on_output. (4dim)
* Searching the best classification
The experiment 3000-2-4-12 gave a good separation of the targets
in featured space. I am proceeding with the whole
classification, then.
Training CNN took 29:17 mn
Each time nets are retrained, the target values change.
Experiment 3000-2-4-12/2dim:
train_score = 0.9986008230452675
test_score = 0.8248148148148148
Experiment 3000-2-4-12/3dim:
train_score = 0.982798353909
test_score = 0.805637860082
* Execution results for including in the report
Repeating the first execution:
- 4000-2-8-24
  - 2dim: NaN values produced.
  - 3dim:
    - Training the CNN took 57:13 mn
    - kNN on train: 1.0
    - kNN on test: 0.85683127572
    - Unfolding:
      - For doing the kNN, make an array containing the values
        of the_samples1 and the_samples2 put together (they
        represent a point) for all the five categories. Then
        make an array of labels that corresponds with it,
        because most probably, the resulting array of points
        will be divided by categories.
      - 
  - 4dim:
    - Training the CNN took 57:27 mn

- 4000-2-4-12
  - 3dim:
    - Unfolding:
      - acc = 0.8808641975308642
