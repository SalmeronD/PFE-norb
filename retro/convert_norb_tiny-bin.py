import leveldb
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

# Make sure that caffe is on the python path:
caffe_root = '/home/diego/.caffe/caffe/'
import sys
sys.path.insert(0, caffe_root + 'python')
import caffe


def putInBatch(datum, data, labels, index1, index2, nid, batch):
    '''Make a pair of ivecs together, and put into the leveldb
    file as a keyvalue label = True or False, indicating a
    similar pair (a dissimilar pair)'''
    ivec = np.hstack(data[[index1, index2], :])
    image = ivec.reshape((datum.height, datum.channels,
    datum.width))
    image = np.swapaxes(image, 0, 1)
    label = int(labels[index1] == labels[index2])    
    datum = caffe.io.array_to_datum(image, label)
    keystr = '{:0>8d}'.format(nid)
    batch.Put(keystr, datum.SerializeToString())


def filterData(data, labels, k):
    '''Keep k samples of data and labels in each class.
    '''
    allIndex = []
    allClass = np.unique(labels)
    for l in allClass:
        theIndex = list(np.where(labels == l))[0]
        allIndex.extend(theIndex[:k])
    labels = labels[allIndex]
    data = data[allIndex, :]
    return data, labels


TRAIN_DATA_FILE = caffe_root + \
'data/norb/smallnorb-5x46789x9x18x6x2x96x96-training-dat.mat'
TRAIN_LABEL_FILE = caffe_root + \
        'data/norb/smallnorb-5x46789x9x18x6x2x96x96-training-cat.mat'
n = 24300
nS = 2

with open(TRAIN_DATA_FILE, 'rb') as f:
    f.seek(24, 0)  # skip the header
    data = np.fromstring(f.read(n*2*96*96), dtype=np.uint8)

with open(TRAIN_LABEL_FILE, 'rb') as f:
    f.seek(20,0)  # skip the header
    labels = np.fromstring(f.read(n*4), dtype=np.int32)

# Pairing the binocular images into one single (96,96*2) image
# Not to be mistaken with the pairing done for the siamese
# architecture input.

binocular_images = np.empty((n, 96, 96*2))
i = np.arange(1, (n*2), 2)
j = (int(np.floor(i_val/2)) for i_val in i)
for i_val in i:
    binocular_images[j.next(), :] = \
    np.concatenate((data[((96*96)*(i_val-1)):(96*96*i_val)].reshape(96,96),
                    data[(96*96*i_val):((96*96)*(i_val+1))].reshape(96,96)),
                    axis = 1)

db_path = 'norb_train_pairs_bin_leveldb'
n_pairs = 10000

# Filtering the data in order to have nS images of each category.
# This allows us to make the tiny phase training.

data, labels = filterData(binocular_images, labels, nS)

# Obtaining the number of similar and dissimilar pairs of
# images. This will help us ordering the data so that minibatches
# take always one within-class pair and R between-class pairs, as
# described in section 5.3.2 of the thesis.  
# nSim is the number of within-class pairs, for all classes. 
# nDis is the number of between-class pairs.

num = len(labels)
nSim = 0
allClass = np.unique(labels)
allIndexes = []
nsample = np.zeros(len(allClass), dtype='int32')
for i, cl in enumerate(allClass):
    nsample[i] = np.sum(labels == cl)
    allIndexes.append(list(np.where(labels == cl))[0])
    nSim += nsample[i]*(nsample[i]-1)/2
nDis = num*(num-1)/2 - nSim
ratio = 1  # Should be int(nDis/nSim)
niters = int(np.ceil(n_pairs/(ratio+1.0)))

# Writing out database

print('Opening db')
if not os.path.exists(db_path):
    os.mkdir(db_path)
datum = caffe.proto.caffe_pb2.Datum()
datum.channels = 2
datum.height = 96
datum.width = 192
db = leveldb.LevelDB(db_path)
batch = leveldb.WriteBatch()
nid = 0
for it in range(niters):
    the_class = np.random.choice(len(allClass), 1, 0)
    the_indexes = np.random.choice(allIndexes[the_class[0]], 2,
                                   0)
    putInBatch(datum, data, labels, the_indexes[0],
               the_indexes[1], nid, batch)
    nid += 1
    for di in range(ratio):
        the_classes = np.random.choice(len(allClass), 2, 0)
        first_index = \
        np.random.choice(allIndexes[the_classes[0]], 1, 0)
        second_index = \
        np.random.choice(allIndexes[the_classes[1]], 1, 0)
        putInBatch(datum, data, labels, first_index,
                   second_index, nid, batch)
        nid += 1
        if nid % 1000 == 0:
            print(str(nid) + 'data passed')
            db.Write(batch, sync=True)
            batch = leveldb.WriteBatch()

print('----------')
if nid % 1000 != 0:
    db.Write(batch, sync=True)
    print('Totally ' + str(nid) + 'data')
