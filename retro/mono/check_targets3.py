import lmdb
import os
import numpy as np
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Make sure that caffe is on the python path:
caffe_root = '/home/diego/.caffe/caffe/'
import sys
sys.path.insert(0, caffe_root + 'python')
import caffe

# Checking an image together with its label in the normal
# training monocular database.

item_id = 2

# Checking the value corresponding to the previous image in the
# target database.

print('Opening lmdb')
tar_path = caffe_root + \
           'pfe/norb/mono/3dim/norb_train_target_lmdb'
if not os.path.exists(tar_path):
    raise Exception(tar_path + ' does not exist.')
mdb_env = lmdb.open(tar_path, map_size=int(1e12))
with mdb_env.begin() as mdb_txn:
    cursor = mdb_txn.cursor()
    keys = []
    datums = []
    targets = np.zeros((5, 3))
    labels = np.zeros(5, dtype=int)
    for i in range(5):
        cursor.get('{:0>8d}'.format(i))
        value = cursor.value()
        keys.append(cursor.key())
        datums.append(caffe.proto.caffe_pb2.Datum())
        datums[i].ParseFromString(value)
        targets[i] = \
                caffe.io.datum_to_array(datums[i]).reshape(3)
        labels[i] = datums[i].label

colors = ['#ff0000', '#ffff00', '#00ff00', '#00ffff', '#0000ff']
categories = ('Animal', 'Human', 'Airplane', 'Truck', 'Car')
fig = plt.figure()
ax = fig.gca(projection='3d')
lines = []
for i in range(5):
    ax.scatter(targets[i, 0], targets[i, 1], targets[i, 2],
               c=colors[i], label=categories[i])
ax.grid()
# fig.legend()
