import numpy as np
import matplotlib.pyplot as plt

from sklearn import neighbors
knn = neighbors.KNeighborsClassifier()

# Make sure that caffe is on the python path:
caffe_root = '/home/diego/.caffe/caffe/'
import sys
sys.path.insert(0, caffe_root + 'python')

import caffe

# decrease if you want to preview during training
iters = 10000
PRETRAINED_FILE = 'CNN3model_iter_' + str(iters) + '.caffemodel'
MODEL_FILE = 'CNN3.prototxt'

caffe.set_mode_cpu()
net = caffe.Net(MODEL_FILE, PRETRAINED_FILE, caffe.TEST)

# ---------read in training data --------#
TRAIN_DATA_FILE = caffe_root + \
'data/norb/smallnorb-5x46789x9x18x6x2x96x96-training-dat.mat'
TRAIN_LABEL_FILE = caffe_root + \
'data/norb/smallnorb-5x46789x9x18x6x2x96x96-training-cat.mat'

n = 24300

with open(TRAIN_DATA_FILE, 'rb') as f:
    f.seek(24, 0)  # Skip the header
    raw_data = np.fromstring(f.read(n*2*96*96), dtype=np.uint8)

with open(TRAIN_LABEL_FILE, 'rb') as f:
    f.seek(20, 0)  # Skip the header
    train_labels = np.fromstring(f.read(n*4), dtype=np.int32)

# Keeping only one image from every pair, in order to carry out a
# monocular classification.

mono_images = np.take(raw_data.reshape(n*2, 96, 96), np.arange(1,
                                                           n*2,
                                                           2), 0)

# Reshape and preprocess
caffe_in = mono_images.reshape(n, 1, 96, 96) * 0.00390625
out = net.forward_all(data=caffe_in)
train_feat = out['ip2']
train_feat = train_feat / np.sqrt(np.sum(train_feat**2,
axis=1))[:, np.newaxis]
train_centers = np.zeros((5, 3))
for i in range(5):
    train_centers[i, :] = np.mean(train_feat[train_labels == i,
:], 0)
train_centers /= \
np.sqrt(np.sum(train_centers**2, axis=1))[:, np.newaxis]

print('----------------')

# ---------Read in test data --------#
TEST_DATA_FILE = caffe_root + \
'data/norb/smallnorb-5x01235x9x18x6x2x96x96-testing-dat.mat'
TEST_LABEL_FILE = caffe_root + \
'data/norb/smallnorb-5x01235x9x18x6x2x96x96-testing-cat.mat'
n = 24300

with open(TEST_DATA_FILE, 'rb') as f:
    f.seek(24, 0)  # Skip the header
    raw_data = np.fromstring(f.read(n*2*96*96), dtype=np.uint8)

with open(TEST_LABEL_FILE, 'rb') as f:
    f.seek(20, 0)  # Skip the header
    test_labels = np.fromstring(f.read(n*4), dtype=np.int32)

# Keeping only one image from every pair, in order to carry out a
# monocular classification.

mono_images = np.take(raw_data.reshape(n*2, 96, 96), np.arange(1,
                                                           n*2,
                                                           2), 0)

# Reshape and preprocess
caffe_in = mono_images.reshape(n, 1, 96, 96) * 0.00390625
out = net.forward_all(data=caffe_in)

test_feat = out['ip2']
test_feat /= np.sqrt(np.sum(test_feat**2, axis=1))[:, np.newaxis]

knn.n_neighbors = 5
print ('start KNN')
print ('training size:')
print (np.shape(train_feat))
print (np.shape(train_labels))
knn.fit(train_feat, train_labels)
acc = knn.score(train_feat, train_labels)
print (acc)

colors = ['#ff0000', '#ffff00', '#00ff00', '#00ffff', '#0000ff']
categories = ('Animal', 'Human', 'Airplane', 'Truck', 'Car')

f = plt.figure()
pics0 = f.add_subplot(211, projection='3d')
pics1 = f.add_subplot(222, projection='3d')
for i in range(5):
    pics0.scatter(train_feat[train_labels == i, 0].flatten(),
                  train_feat[train_labels == i, 1].flatten(),
                  train_feat[train_labels == i, 2].flatten(),
                  '.', c=colors[i], label=categories[i])
    pics0.text(train_centers[i, 0].flatten(), 
               train_centers[i, 1].flatten(),
               train_centers[i, 2].flatten(), categories[i])
    pics1.scatter(test_feat[test_labels == i, 0].flatten(),
                  test_feat[test_labels == i, 1].flatten(),
                  test_feat[test_labels == i, 2].flatten(), '.',
                  c=colors[i], label=categories[i])
    pics[1].legend(numpoints=1, fontsize=8)
plt.grid()
plt.savefig('CNN3_'+str(iters) + '.png')
plt.show()


print ('testing size:')
print (np.shape(test_feat))
print (np.shape(test_labels))
acc = knn.score(test_feat, test_labels)
print (acc)
