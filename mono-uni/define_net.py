import sys
import os
import subprocess

# This function must take four arguments, in this order: N. of
# iterations in the tiny phase, n. of samples in the tiny phase,
# number of feature extractors in conv1 and number of feature
# extractors in conv2.


def commands(dim, tiny_iter, tiny_samples, conv1_extract,
             conv2_extract):
    if dim < 2 or dim > 4:
        raise Exception("Dimension value must be between 2 and \
    4")
    caffe_root = "/home/diego/.caffe/caffe"
    mold_path = caffe_root + "/pfe/norb/mold"
    cmd = []

    # tsmlCNN_train.prototxt
    sed_cmd = '43s/8/' + conv1_extract + '/g;79s/24/' + \
    conv2_extract + '/g;167s/8/' + conv1_extract + \
    '/g;203s/24/' + conv2_extract + '/g'
    cmd.append('sed -e "' + sed_cmd + '" ' + mold_path + '/' +
               str(dim) + 'dim/tsmlCNN_train.prototxt > \
               tsmlCNN_train.prototxt')

    # tsmlCNN.prototxt
    sed_cmd = '19s/8/' + conv1_extract + '/g;47s/24/' + \
    conv2_extract + '/g'
    cmd.append('sed -e "' + sed_cmd + '" ' + mold_path + '/' +
               str(dim) + 'dim/tsmlCNN.prototxt > \
               tsmlCNN.prototxt')

    # tsmlCNN_solver.prototxt
    sed_cmd = '14,16s/5000/' + tiny_iter + '/g'
    cmd.append('sed -e "' + sed_cmd + '" ' + mold_path +
               '/' + str(dim) + 'dim/tsmlCNN_solver.prototxt > \
               tsmlCNN_solver.prototxt')

    # train_tsmlCNN.sh
    cmd.append('cp ' + mold_path +
                   '/' + str(dim) + 'dim/train_tsmlCNN.sh .')

    # form_train_target.py
    sed_cmd = '27s/5000/' + tiny_iter + '/g;28s/2/' + \
    tiny_samples + '/g'
    cmd.append('sed -e "' + sed_cmd + '" ' + mold_path +
               '/' + str(dim) + 'dim/form_train_target.py > \
               form_train_target.py')

    #  check_targets.py
    cmd.append('cp ' + mold_path + '/' + str(dim) +
               'dim/check_targets.py .')

    # CNN_train.prototxt
    sed_cmd = '43s/8/' + conv1_extract + '/g;77s/24/' + \
    conv2_extract + '/g'
    cmd.append('sed -e "' + sed_cmd + '" ' + mold_path +
               '/' + str(dim) + 'dim/CNN_train.prototxt > \
               CNN_train.prototxt')

    # CNN.prototxt
    sed_cmd = '19s/8/' + conv1_extract + '/g;53s/24/' + \
    conv2_extract + '/g'
    cmd.append('sed -e "' + sed_cmd + '" ' + mold_path +
               '/' + str(dim) + 'dim/CNN.prototxt > CNN.prototxt')

    # CNN_solver.prototxt
    cmd.append('cp ' + mold_path +
               '/' + str(dim) + 'dim/CNN_solver.prototxt .')

    # train_CNN.sh
    sed_cmd = '4s/5000/' + tiny_iter + '/g'
    cmd.append('sed -e "' + sed_cmd + '" ' + mold_path +
               '/' + str(dim) + 'dim/train_CNN.sh > train_CNN.sh')

    # draw_final_shape.py
    if dim == 4:
        cmd.append('cp ' + mold_path +
                   '/4dim/knn_on_output.py .')
    else:
        cmd.append('cp ' + mold_path +
                   '/' + str(dim) + 'dim/draw_final_shape.py .')

    # unfold_test.py
    cmd.append('cp ' + mold_path +
               '/' + str(dim) + 'dim/unfold_test.py .')

    return cmd


def define_net(tiny_iter, tiny_samples, conv1_extract,
               conv2_extract, cnb=False, dim2=False, dim3=False,
               dim4=False):
    if (tiny_iter < 1000 or tiny_iter > 10000):
        print("Tiny iterations must be in the range 1000-10000")
    elif (tiny_samples < 2 or tiny_samples > 10):
        print("Tiny samples must be in range 2-10")
    elif (conv1_extract < 2 or conv1_extract > 10):
        print("conv1 extractors must be in range 2-10")
    elif (conv2_extract < 6 or conv2_extract > 30):
        print("conv2 extractors must be in range 6-30")
    else:
        tiny_iter = str(tiny_iter)
        tiny_samples = str(tiny_samples)
        conv1_extract = str(conv1_extract)
        conv2_extract = str(conv2_extract)

        folder = tiny_iter + '-' + tiny_samples + '-' \
                 + conv1_extract  + '-' + conv2_extract

        caffe_root = "/home/diego/.caffe/caffe"
        mold_path = caffe_root + "/pfe/norb/mold"
        mono_uni = caffe_root + "/pfe/norb/mono-uni"
        os.chdir(mono_uni)

        if not os.path.exists(folder):
            # convert_norb_tiny.py
            cmd_ncb = 'sed -e "43s/2/' + tiny_samples + '/g" ' + \
            mold_path + '/convert_norb_tiny.py > \
            convert_norb_tiny.py'

            os.mkdir(folder)
            os.chdir(folder)
            try:
                subprocess.check_call(cmd_ncb, shell=True)
            except subprocess.CalledProcessError:
                print("Failed command " + cmd_ncb + " in \
                directory " + os.getcwd())
            else:
                pass

            cmd = commands(2, tiny_iter, tiny_samples,
                           conv1_extract, conv2_extract)
            os.mkdir('2dim')
            os.chdir('2dim')
            for cmd in cmd:
                try:
                    subprocess.check_call(cmd, shell=True)
                except subprocess.CalledProcessError:
                    print("Failed command " + cmd + " in \
                           directory " + os.getcwd())
                    break
                else:
                    continue

            cmd = commands(3, tiny_iter, tiny_samples,
                           conv1_extract, conv2_extract)
            os.chdir(mono_uni + '/' + folder)
            os.mkdir('3dim')
            os.chdir('3dim')
            for cmd in cmd:
                try:
                    subprocess.check_call(cmd, shell=True)
                except subprocess.CalledProcessError:
                    print("Failed command " + cmd + " in \
                           directory " + os.getcwd())
                    break
                else:
                    continue

            cmd = commands(4, tiny_iter, tiny_samples,
                           conv1_extract, conv2_extract)
            os.chdir(mono_uni + '/' + folder)
            os.mkdir('4dim')
            os.chdir('4dim')
            for cmd in cmd:
                try:
                    subprocess.check_call(cmd, shell=True)
                except subprocess.CalledProcessError:
                    print("Failed command " + cmd + " in \
                           directory " + os.getcwd())
                    break
                else:
                    continue

        else:
            os.chdir(folder)
            if cnb is True:
                # convert_norb_tiny.py
                cmd_ncb = 'sed -e "43s/2/' + tiny_samples + \
                '/g" ' + mold_path + '/convert_norb_tiny.py > \
                convert_norb_tiny.py'

                try:
                    subprocess.check_call(cmd_ncb, shell=True)
                except subprocess.CalledProcessError:
                    print("Failed command " + cmd_ncb + " in \
                    directory "+ os.getcwd())
                else:
                    pass

            if dim2 is True:
                cmd = commands(2, tiny_iter, tiny_samples,
                               conv1_extract, conv2_extract)
                os.chdir(mono_uni + '/' + folder)
                if not os.path.exists('2dim'):
                    os.mkdir('2dim')
                os.chdir('2dim')
                for cmd in cmd:
                    try:
                        subprocess.check_call(cmd, shell=True)
                    except subprocess.CalledProcessError:
                        print("Failed command " + cmd + " in \
                               directory " + os.getcwd())
                        break
                    else:
                        continue

            if dim3 is True:
                cmd = commands(3, tiny_iter, tiny_samples,
                               conv1_extract, conv2_extract)
                os.chdir(mono_uni + '/' + folder)
                if not os.path.exists('3dim'):
                    os.mkdir('3dim')
                os.chdir('3dim')
                for cmd in cmd:
                    try:
                        subprocess.check_call(cmd, shell=True)
                    except subprocess.CalledProcessError:
                        print("Failed command " + cmd + " in \
                               directory " + os.getcwd())
                        break
                    else:
                        continue

            if dim4 is True:
                cmd = commands(4, tiny_iter, tiny_samples,
                               conv1_extract, conv2_extract)
                os.chdir(mono_uni + '/' + folder)
                if not os.path.exists('4dim'):
                    os.mkdir('4dim')
                os.chdir('4dim')
                for cmd in cmd:
                    try:
                        subprocess.check_call(cmd, shell=True)
                    except subprocess.CalledProcessError:
                        print("Failed command " + cmd + " in \
                               directory " + os.getcwd())
                        break
                    else:
                        continue


if __name__ == "__main__":
    define_net(*sys.argv[1:])
