import os
from glob import glob

from run_net import run_net

# This script is to be run from the folder inmediately superior to
# all the experiment folders.

db_dir = '/home/diego/.caffe/caffe/pfe/norb/mono-uni'

dirs = glob('[0-9]*')
dirs.sort()

for directory in dirs:
    os.chdir(db_dir)
    run_net(directory, dim4=True)
