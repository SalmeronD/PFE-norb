import subprocess
import os


def run_net(directory, cnb=False, dim2=False, dim3=False,
            dim4=False):
    """
    Executes every piece of code until obtaining the target
    database.
    Allows executing only certain parts of the code by setting to
    True the four optional arguments.
    If run as a script, it must be run from the directory containing the
    code, which must be structured as follows:
    - convert_norb_tiny.py
    - 2dim
      - train_tsmlCNN2.sh
      - The net and solver definitions
      - form_train_target2.py
      - check_targets.py
    - 3dim
      - ...
    - 4dim
      - ...
    """

    # convert_norb_tiny.py
    cmd_cnb = 'python convert_norb_tiny.py'

    cmd = []

    # train_tsmlCNN.sh
    cmd.append('./train_tsmlCNN.sh')
    
    # form_train_target.py
    cmd.append('python form_train_target.py')

    # check_targets.py
    cmd.append('python check_targets.py')

    caffe_root = "/home/diego/.caffe/caffe"
    mono_uni = caffe_root + "/pfe/norb/mono-uni"

    if cnb is True:
        os.chdir(mono_uni + '/' + directory)
        print("Now in directory " + mono_uni + "/" + directory)
        try:
            subprocess.check_call(cmd_cnb, shell=True)
        except subprocess.CalledProcessError:
            print("Failed command " + cmd_cnb + " in directory " +
                  os.getcwd())
        else:
            pass

    if dim2 is True:
        os.chdir(mono_uni + '/' + directory)
        if not os.path.exists('2dim'):
            print("There are no net definitions for the \
            two-dimensional case")
        else:
            os.chdir('2dim')
            print("Now in directory " + mono_uni + "/" +
                  directory + "/2dim")
            for cmd in cmd:
                try:
                    subprocess.check_call(cmd, shell=True)
                except subprocess.CalledProcessError:
                    print("Failed command " + cmd + " in \
                    directory " + os.getcwd())
                    break
                else:
                    continue
    if dim3 is True:
        os.chdir(mono_uni + '/' + directory)
        if not os.path.exists('3dim'):
            print("There are no net definitions for the \
            two-dimensional case")
        else:
            os.chdir('3dim')
            print("Now in directory " + mono_uni + "/" +
                  directory + "/3dim")
            for cmd in cmd:
                try:
                    subprocess.check_call(cmd, shell=True)
                except subprocess.CalledProcessError:
                    print("Failed command " + cmd + " in \
                    directory " + os.getcwd())
                    break
                else:
                    continue

    if dim4 is True:
        os.chdir(mono_uni + '/' + directory)
        if not os.path.exists('4dim'):
            print("There are no net definitions for the \
        two-dimensional case")
        else:
            os.chdir('4dim')
            print("Now in directory " + mono_uni + "/" +
                  directory + "/4dim")
            for cmd in cmd:
                try:
                    subprocess.check_call(cmd, shell=True)
                except subprocess.CalledProcessError:
                    print("Failed command " + cmd + " in \
                    directory " + os.getcwd())
                    break
                else:
                    continue

if __name__ == "__main__":
    import sys
    run_net(*sys.argv[1:])
