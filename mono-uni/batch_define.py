from define_net import define_net

tiny_iters = range(2000, 5000, 1000)
tiny_samples = range(2, 6, 2)
conv1_extract = range(2, 10, 2)
conv2_extract = range(6, 30, 6)

# Values for conv1_extract and conv2_extract proposed in LeNet7
# are 8 and 24, respectively. To keep this proportion the step of
# conv2 is three times that of conv1.

# Only those cases where conv2_extract is three times
# conv1_extract are preserved, in order to keep the similarity
# with LeNet7 and to reduce the number of cases

[define_net(iters, samples, conv1, conv2, dim4=True) for iters in
 tiny_iters for samples in tiny_samples for conv1 in
 conv1_extract for conv2 in conv2_extract if conv2 == 3*conv1]
