import numpy as np
import matplotlib.pyplot as plt
from glob import glob
import os


def dist(a, b):
    dist = (a-b)**2
    dist = dist.sum(axis=-1)
    dist = np.sqrt(dist)
    return dist


def dist_to_points(collection):
    n_points = len(collection)
    distances = np.zeros((n_points, n_points))
    for i in range(n_points):
        for j in range((i + 1), n_points):
            distances[i, j] = dist(collection[i], collection[j])
    distances = distances + distances.T
    distances = np.asmatrix(distances)
    return distances


def deviations(distances):
    distances.sort()
    distances = distances[:, 1:3]
    within_std = np.std(distances, axis=1)
    between_std = np.std(distances, axis=0)
    within_mean = np.mean(within_std)
    between_mean = np.mean(between_std)
    return within_mean, between_mean


def best_std(all_stds, n=3, within_coeff=0.5, between_coeff=0.5):
    '''
    Given a list of tuples containing the mean of the within and
    between-deviations, calculates the 'n' best deviations.

    Returns the indexes they are occupying in the matrix.
    '''

    std = np.zeros(len(all_stds))
    for i in range(len(all_stds)):
        std[i] = within_coeff * all_stds[i][0] + \
                     between_coeff * all_stds[i][1]
    indexes = np.argsort(std)[:n]
    return indexes


def std_all_targets(iters='*', samples='*', conv1='*',
                    conv2='*'):
    mono_uni = '/home/diego/.caffe/caffe/pfe/norb/mono-uni'
    os.chdir(mono_uni)
    dirs = glob(str(iters) + '-' + str(samples) + '-' +
                str(conv1) + '-' + str(conv2))
    dirs.sort()

    all_stds = []
    for directory in dirs:
        targets = np.load(directory + '/2dim/targets.npy')
        distances = dist_to_points(targets)
        all_stds.append(deviations(distances))

    best_indexes = best_std(all_stds)
    best_dirs = dirs[best_indexes]
    print("Best experiments correspond to " +
          str(best_dirs))


def plot_targets(axis, targets):
    colors = ['#ff0000', '#ffff00', '#00ff00', '#00ffff',
              '#0000ff']
    categories = ('Animal', 'Human', 'Airplane', 'Truck', 'Car')
    for i in range(5):
        axis.plot(targets[i, 0], targets[i, 1], 'o', c=colors[i],
                  label=categories[i])
    axis.axis([-1.5, 1.5, -1.5, 1.5])
    axis.grid()
    # plt.legend(numpoints=1)


def plot_all_targets(iters='*', samples='*', conv1='*',
                     conv2='*'):
    mono_uni = '/home/diego/.caffe/caffe/pfe/norb/mono-uni'
    os.chdir(mono_uni)

    dirs = glob(str(iters) + '-' + str(samples) + '-' +
                str(conv1) + '-' + str(conv2))
    dirs.sort()

    fig, axes = plt.subplots(2, len(dirs)/2, sharex='col',
                             sharey='row', figsize=(16, 9))
    fig.suptitle(str(iters) + '-' + str(samples) + '-' +
                 str(conv1) + '-' + str(conv2))

    step = 0
    for ax in axes.flatten():
        directory = dirs[step]
        attr = directory.split('-')
        iters_u = attr[0]
        samples_u = attr[1]
        conv1_u = attr[2]
        conv2_u = attr[3]
        ax.set_title(iters_u + " iters\n" +
                     samples_u + " samples\n" +
                     conv1_u + " first feature extractors\n" +
                     conv2_u + " second feature extractors",
                     fontsize='x-small')
        targets = np.load(directory + '/2dim/targets.npy')
        plot_targets(ax, targets)
        step += 1

    plt.savefig('targets_' + str(iters) + '-' + str(samples) +
                '-' + str(conv1) + '-' + str(conv2) + '.png')
    plt.show()

if __name__ == "__main__":
    import sys

    plot_all_targets(*sys.argv[1:])
