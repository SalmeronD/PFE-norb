import os
import subprocess
from glob import glob

db_dir = '/home/diego/.caffe/caffe/pfe/norb/mono-uni'

dirs = glob('[0-9]*')
dirs.sort()

for directory in dirs:
    os.chdir(directory + '/2dim')
    subprocess.call('python check_targets.py', shell=True)
    os.chdir(db_dir)
